#!/bin/bash
# "This script reads from file, and writes the data into the data base"
# The data base table 'country' will be populated with country code and 
# country code. See file 'allCountries'

allcount=/home/henning/nextcloud/WorkingSpace/sql/ImportCountries/allCountries
 
while IFS= read -r line
do
    (echo "INSERT INTO country (name, code) VALUES( $line);" | mariadb -uhenning -p'passord' ws_dev)done < "$allcount"
echo "Table country updated."
