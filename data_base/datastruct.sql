



          #########################################
          #                                       #
          #   !!! DELETES ALL TABLES !!!          #
          #           in ws_dev                   #
          #   and rebuilds the structure          #
          #                                       #
          #########################################


# show tables;
# show databases;
use ws_dev;

DROP TABLE IF EXISTS visited;
DROP TABLE IF EXISTS ip;
# DROP TABLE IF EXISTS country ;
DROP TABLE IF EXISTS page;


CREATE TABLE country 
(	code varchar(32),
	name varchar(255),
	PRIMARY KEY (code)
);

CREATE TABLE ip
(	ip varchar(255),
	code varchar(16),
	region varchar(255),
	PRIMARY KEY (ip),
	CONSTRAINT `fk_country`
	    FOREIGN KEY (code) REFERENCES country (code)
	    ON DELETE CASCADE
	    ON UPDATE RESTRICT
);

CREATE TABLE page
(	page_name varchar(255),
	last_count int,
	last_update datetime,
	PRIMARY KEY (page_name)
);

CREATE TABLE visited
(	id int AUTO_INCREMENT,
	ip varchar(255),
	page varchar(255),
	visit_date datetime,
	PRIMARY KEY (id)
);

