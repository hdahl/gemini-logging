#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 12:40:40 2023

@author: henning

Main script file:
Objective:
    1. Read from file and pick up disired data based on spesified surrinding text.
    2. INSERT INTO mariadb

"""

# The textfile, '.agate_variables', is a copy of lines in the journalctl log.
# Temp text file read the json file:

import myFunctions

# json
from configparser import ConfigParser
config = ConfigParser()
config.read('.agate_variables')
journal_file = config.get('files', 'logfile')
rootdoc = config.get('files', 'rootdoc')
_date=time=ip_addr=file_name='0'

myFunctions.writeJournalToFile(journal_file)
myFunctions.cleanUpJournalfile(journal_file , journal_file , 'WARN|unexpectedly|error' )
# From my module myFunctions returns mariadb.connection
conn = myFunctions.conn()
cur = conn.cursor()
# Open file 
rawText = open(journal_file)
# Using readlines()
Lines = rawText.readlines() #Reads whole file into memory with \n
count = 0
region = country = None
# Strips the newline character
command = date = time = ip_addr = '0'
for line in Lines:
    count += 1
    # Pick data (Date, ip address and filename). I don't need country code here
    tmp = line.find('.gmi')
    # File name:
    if tmp == -1:
        file_name = rootdoc
    else:
        file_name = line[line.find('no/')+3:line.find('.gmi')+4]
    # IP address
    ip_addr = line[line.find('1965 ')+5:line.find(' "')]
    # Date
    date = line[line.find(': [')+3:line.find('T')]
    # Time
    time = line[line.find('T')+1:line.find('Z')]
   
    # Data picked up are to be populate in db table ws_dev
    # Get Cursor. Cursor is used to execute statments to communicate with mariadb data base.
    command = 'INSERT INTO visited (ip, page, visit_date) values ("'
    command = command+ip_addr +'", "'+file_name+'", "'+date+' '+time+'");'
    cur.execute(command)
    # INSERT statement must be commited
    conn.commit()
    # Getting country code and region
    region, country = myFunctions.countryRegionFromWeb(ip_addr)
#    print(region + ' ' + country)
    # insert into tbl ip
    command = "INSERT IGNORE INTO ip (ip, code, region)  VALUES ("
    command += "'"+ip_addr +"', '"+ country +"', '"+ region +"');"    
    cur.execute(command)
    conn.commit()
myFunctions.updateTblPage(conn)
#file = '/home/henning/nextcloud/WorkingSpace/python/txtFileToMaipulate'
myFunctions.updateTotalCount()
print('done')






1




