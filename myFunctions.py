#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 19:05:46 2023

@author: henning

Functions used by dataFromJournalctlTextFile.py:

"""
def conn():
    '''
    Establishing connection to the database server defined in the .agate_variables
    file, and return connection to the DB.
    '''
    from configparser import ConfigParser
    config = ConfigParser()
    iniFile = '.agate_variables'
    try:
        f = open(iniFile)
        f.close()
    except FileNotFoundError:
        print('File does not exist')
        return None
#        print("File Closed")
    config.read(iniFile)
    
    _dbserver =  config.get('db', 'dbserver')
    _user = config.get('db', 'user')
    _pw = config.get('db', 'pw')
    _db = config.get('db', 'db')

    import mariadb
    import sys
    conn = None
    # Connect to MariaDB Platform
    try:
        conn = mariadb.connect(
            user=_user,
            password=_pw,
            host=_dbserver,
            port=3306,
            database=_db
        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)
        return -1
    return conn

def countryRegionFromWeb(ipAddr):
    '''
    Return tuplet: a, b = functionReturnsTwoValues. e.g. return x, y
    Returns country and region
    '''
    import requests
    import re
    response = requests.get('http://ipinfo.io/' + ipAddr)
    rawtxt = response.content
    #Region
    pattern = bytes('"region": "(.*)",', encoding='utf8')
    #print('region:')
    region = re.findall(pattern , rawtxt)
    txt = b''
    for x in region:
        txt += x
    # converting
    region = txt.decode()
    #Country
    pattern = bytes('"country": "(.*)",', encoding='utf8')
    #print('region:')
    country = re.findall(pattern , rawtxt)
    txt = b''
    for x in country:
        txt += x
    # converting
    country = txt.decode()
#    print('Country: '+ country)
#   print('Region: ' + region)
    return region, country

def updateTblPage(conn):
    cur = conn.cursor()
    # Counting records in tbl visited, and updates tbl page
    query = " UPDATE page lu "
    query += "SET lu.last_count = ("
    query += "    SELECT count(*) count"
    query += "    FROM visited gll"
    query += "    WHERE lu.page_name = gll.page"
    query += "    ),"
    query += "    last_update = CURRENT_TIMESTAMP()"
    query +=";"
    cur.execute(query)
    conn.commit()
    # Inserting new 
    # Setter inn alle linjer som ikke finns der allerede:
    query = "insert into page (last_count, page_name) "
    query += "Select count(*) count, page page "
    query += "FROM visited "
    query += "WHERE page not in (select page_name from page) " 
    query += "GROUP BY page;"
    cur.execute(query)
    conn.commit()

def writeCountToGmi(file, count):
    
    with open(file , 'r+') as fp:
        # read an store all lines into list
        lines = fp.readlines()
        # move file pointer to the beginning of a file
        fp.seek(0)
        # truncate the file
        fp.truncate()
    
        # start writing lines except the last line
        # lines[:-1] from line 0 to the second last line
        fp.writelines(lines[:-1])
        fp.writelines(str(count))  
    
def writeJournalToFile(logfile):
    import time
    from systemd import journal
    
    #logfile = 'result'
    yesterday = time.time() - 24 * 60**2

    j = journal.Reader()
    j.seek_realtime(yesterday)
    j.this_boot()
    j.log_level(journal.LOG_INFO )
    j.log_level(3) # Why do I have this one?
    j.add_match('_EXE=/home/gemini/bin/agate')
    txtline = ''
    for entry in j:                                 
        txtline += entry['MESSAGE']+'\n'
    file = open(logfile , "w")
    file.write(txtline)
    file.close()

def updateTotalCount():
    '''
    DESCRIPTION.
    Updates files. Edits the last line based on sql tbl. 

    Returns
    -------
    None.

    '''
    import myFunctions

    # json variables, rootpath 
    from configparser import ConfigParser
    config = ConfigParser()
    iniFile = '.agate_variables'
    try:
        f = open(iniFile)
        f.close()
    except FileNotFoundError:
        print('File does not exist')
    #    return None
    #        print("File Closed")
    config.read(iniFile)

    _rootpath = config.get('files', 'rootpath')
    #print((_rootpath))

    # read file from tbl.page + full file path
    query = 'Select page_name, last_count from page'
    conn = myFunctions.conn()
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()
    result = cur.fetchall()
    for line in result:
        page = line[0]
        count = line[1]
        file = _rootpath+page
        myFunctions.writeCountToGmi(file, count)

def cleanUpJournalfile(sourceFile , targetFile , searchString):
    '''
    Parameters
    ----------
    sourceFile : TYPE string
        DESCRIPTION. Name of file holding lines desired (among others).
    targetFile : TYPE string
        DESCRIPTION. Name of file where the desired lines will be written.
                     targetFile = sourceFile is valid
    searchString : TYPE regex string
        DESCRIPTION. Desired lines has not this string in it.

    Returns
    -------
    sourceFile : TYPE string
        DESCRIPTION. File name to source file.
    targetFile : TYPE string
        DESCRIPTION. File name to file where the result

    '''
    import re
    readFile = open(sourceFile) # Read from this
    txt = ''                    # String to be written
    result = targetFile         # Write to this
    for line in readFile :
    #   print(line)
       m = re.search(searchString , line)
       if m == None:
           txt += line 
    readFile.close()
    with open(result , 'r+') as fp:
        # move file pointer to the beginning of a file
        fp.seek(0)
        # truncate the file
        fp.truncate()
        fp.write(txt)
    return sourceFile, targetFile

    '''
    searchString = 'INFO'
    print('Honk the horn!')
    file1, file2 = cleanUpJournalfile('testjournalWARN', 'testjournalWARN', searchString)
    print('The file "'+file1+'" is cleaned up and is written to the file "'+file2+'".')
    print(file2+' has only lines includding the string "'+ searchString+'".')
    '''


